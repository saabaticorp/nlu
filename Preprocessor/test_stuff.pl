$c = 'soap dishes &amp; dispensers';
$t = 'bay pottery top cat lotion or soap dispenser pink ceramic';

@title = split(/\s+/,$t);
foreach (@title){
  $title{$_}++;
}
                
for my $ct (split(/\s+/,$c)){
  my $cint = check_categ_in_title($ct,%title);
  if ($cint){
  	print "$cint\n";
    $spec{"type"} .= "$cint ";
  }
}
if (exists $spec{"type"}){
   chop $spec{"type"};
   print $spec{"type"},"\n";
}           



sub check_categ_in_title{
    my ($c,%title) = @_;
    if (exists $title{$c}){
        return $c;
    }
    elsif (exists $title{"${c}s"}){
        return "${c}s";
    }
    elsif ($c=~/s$/ and chop $c and exists $title{$c}){
        #chop updates $c!
        return $c;
    }
    return "";
}