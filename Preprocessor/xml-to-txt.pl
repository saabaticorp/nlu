

use strict;

my %supportedSpecificsMap = (
    "Brand" => "brand",
    "Color" => "color",
    "Style" => "style",
    "Type" => "stype",
    "Material" => "material",
    "Country/Region of Manufacture" => "manufactured",
    "Model" => "model",
    "Gender" => "gender",
    "Size" => "size",
    "Pattern" => "pattern",
    "Features" => "features",
    "Warranty" => "warranty",
    "Metal" => "metal",
    "Screen Size" => "size",
    "Width" => "width",
    "US Shoe Size (Men's)" => "size",
    "Size (Men's)" => "size",
    "Year" => "year",
    "Surface Finish" => "material",
    "Metal Purity" => "purity",
    "US Shoe Size (Women's)" => "size",
    "Storage Capacity" => "memory",
    "Product" => "product",
    "Team" => "team",
    "Size (Women's)" => "size",
    "Theme" => "theme",
    "Placement on Vehicle" => "placement",
    "Platform" => "platform",
    "Compatible Brand" => "compatible",
    "Heel Height" => "height",
    "Sleeve Length" => "length",
    "Operating System" => "os",
    "Bottoms Size (Men's)" => "size",
    "Country of Manufacture" => "manufactured",
    "Main Stone Shape" => "shape",
    "Product Line" => "model",
    "Room" => "room",
    "Main Stone" => "stone",
    "Memory" => "memory",
    "Processor Speed" => "speed",
    "Hard Drive Capacity" => "disk",
    "Network" => "network",
    "Recommended Age Range" => "age",
    "Sport" => "sport",
    "Processor Type" => "processor",
    "Band Material" => "material",
    "Camera Resolution" => "resolution",
    "Frame Material" => "material",
    "Connectivity" => "connectivity",
    "Occasion" => "occasion",
    "Bag Length" => "size",
    "Series" => "model",
    "Length (inches)" => "size",
    "Release Year" => "year",
    "Main Stone Color" => "color",
    "Bag Depth" => "size",
    "Fit Design" => "compatible",
    "Total Carat Weight (TCW)" => "size",
    "Dexterity" => "compatible",
    "Band Color" => "color",
    "Strap Drop" => "size",
    "Earpiece Design" => "earpiece"
);
#    "UPC" => "upc",
#    "Bundled Items" => "bundled_items",
#    "Fastening" => "fastening",
#    "Inseam" => "inseam",
#    "Interchange Part Number" => "interchange_part_number",
#    "Character Family" => "character_family",
#    "Circulated/Uncirculated" => "circulated/uncirculated",
#    "Movement" => "movement",

#some stats
my ($numItems, $itemsKept) = (0,0);
while(<>){
	if (/<Item>/){
		$numItems++;
		my ($id, $title, $categ, $goodSpecs) = ("","", 0);
		my %spec = ();
		my %title = ();
		my @title = ();
		while($_=<> and $_!~/<\/Item>/){
			if (/ItemId>(\d+)/){
				$id = $1;
			}
			elsif (/<Title>(.+)<\/Title>/){
				$title = preprocess($1);
				@title = split(/\s+/,$title);
				foreach (@title){
					$title{$_}++;
				}
			}
			elsif (/<CategoryName>(.+)<\/CategoryName>/){
				$categ = preprocess($1);				
			}
			elsif (/<Specific>/){
				# sanity checks
				if (/<Specific>(.+?):\s+(.+)<\/Specific>/){
					my ($key,$value) = ($1,$2);
					$value = preprocess($value);
					if (exists $supportedSpecificsMap{$key}){
						my $vt = spec_in_title($value,%title);
						if ( $vt ){
							if (! exists $spec{$supportedSpecificsMap{$key}}){
							 $spec{$supportedSpecificsMap{$key}} = $value;
							 $goodSpecs++;
							}
							else{
								#if equal continue
                                my $old = $spec{$supportedSpecificsMap{$key}};
                                if ($old eq $value){
                                    next;
                                }
                                elsif (index($title, "$old $value") != -1){
                                	$spec{$supportedSpecificsMap{$key}} .= " $value";
                                }
                                elsif (index($title, "$value $old") != -1){
                                    $spec{$supportedSpecificsMap{$key}} = "$value $old";
                                }
                                else{
                                	#if both appear, print to inspect what to do
                                    print STDERR "$id\tkey=[$key], old=[$spec{$supportedSpecificsMap{$key}}], new=[$value]\t$title\n";
                                }
							}
						}
					}
				}
				else{
					print STDERR "unexpected spec format in id=$id: $_\n";
				}
			}
		}
		
		#check if we can find a type
		if (! exists $spec{"type"}){
			for my $ct (split(/\s+/,$categ)){
	            my $cint = check_categ_in_title($ct,%title);
	            if ($cint){
	               $spec{"type"} .= "$cint ";
	            }
	        }
	        if (exists $spec{"type"}){
	            chop $spec{"type"};
	            # print "type=", $spec{"type"}, "\n";
	            $goodSpecs++;
	        }
		}
                
		if ($goodSpecs){
			$itemsKept++;
			print "$id\t$categ\t$title\n";
			foreach my $key (keys %spec){
				print "\t$key\t$spec{$key}\n";
			}
			print "\n";
		}
	}
}

print STDERR "items=$numItems, kept=$itemsKept\n";




sub check_categ_in_title{
    my ($c,%title) = @_;
    if (exists $title{$c}){
    	return $c;
    }
    elsif (exists $title{"${c}s"}){
    	return "${c}s";
    }
    elsif ($c=~/s$/ and chop $c and exists $title{$c}){
    	#chop updates $c!
    	return $c;
    }
    return "";
}


sub spec_in_title{
	my ($spec,%title) = @_;
	# currently, this will only check if one element is in title it's good enough
	foreach (split(/\s+/,$spec)){
		if ( check_categ_in_title($_, %title ) ){
            return 1;
        }
	}	
	return 0;
}


# from moses preprocess
# https://github.com/kpu/preprocess/blob/master/preprocess/moses/tokenizer/tokenizer.perl
sub preprocess{
	my $text = shift;
    $text = lc($text);
    # remove ASCII junk
    $text =~ s/\s+/ /g;
    $text =~ s/[\000-\037]//g;
    
    # seperate out all "other" special characters
    $text =~ s/silk~y/silky/;    
    $text =~ s/[(){},:!?\[\]~^]/ /g;    
    $text =~ s/w\// with /g;
    # $text =~ s/([:!?])/ $1 /g;
    
    # turn `into '
    $text =~ s/[`’]/\'/g;
    #turn '' into "
    $text =~ s/\'\'/ \" /g;
    
    # clean up extraneous spaces
    $text =~ s/ +/ /g;
    $text =~ s/^ //g;
    $text =~ s/ $//g;
    return $text;
}