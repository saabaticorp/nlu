use utf8;

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");

while(<>){
	print preprocess($_), "\n";
	#if ($_=~/[-',\/\$\.&"!*\)\(%_~#:\+\|\?@]/){
    #    print "$_";
    #}    
}

sub preprocess{
    $text = shift;
    $text = lc($text);
    # remove ASCII junk
    $text =~ s/\s+/ /g;
    $text =~ s/[\000-\037]//g;
    
    # seperate out all "other" special characters
    $text =~ s/silk~y/silky/;    
    $text =~ s/[(){},:!?\[\]~^]/ /g;    
    $text =~ s/w\// with /g;
    # $text =~ s/([:!?])/ $1 /g;
    
    # turn `into '
    $text =~ s/[`’]/\'/g;
    #turn '' into "
    $text =~ s/\'\'/ \" /g;
    
    # clean up extraneous spaces
    $text =~ s/ +/ /g;
    $text =~ s/^ //g;
    $text =~ s/ $//g;
    return $text;
}