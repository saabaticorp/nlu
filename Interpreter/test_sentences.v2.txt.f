################################
#refine intent
only show red shoes ||| refine red shoes
show only red shoes ||| refine red shoes
only show nike ||| refine nike
show only nike ||| refine nike
just show black ones ||| refine black
just show me black ones ||| refine black
Please just show me green ones ||| refine green
Please just show green ones ||| refine green
Can you	just show yellow ones ||| refine yellow
Can you	just show me yellow ones ||| refine yellow
I only want to see Canvas shoes ||| refine Canvas shoes
I only want to see Cotton shoes ||| refine Cotton shoes
I only want to see Denim shoes ||| refine Denim shoes
I only want to see Real Leather ||| refine Real Leather
Just show me red ones ||| refine red
limit the color to red ||| refine color to red
#show only under 40 dollars ||| price [0-40]
#show only below 40 dollars ||| price [0-40]
#show only less than 40 dollars ||| price [0-40]
#show only above 40 dollars ||| price [40-]
#show only higher than 40 dollars ||| price [40-]
#show only more than 40 dollars ||| price [40-]
#show only between 40 and 60 dollars ||| price [40-60]
#show only price between 40 and 60 dollars ||| price [40-60]
#Please show only under 50 dollars ||| price [0-50]
#Just show only under 60 dollars ||| price [0-60]
#Can you	show only under 70 dollars ||| price [0-70]
################################
#search intent
Birkenstock sandals for men size 12 ||| find Birkenstock sandals for men size 12
find some good nike shoes ||| find nike shoes
find good sexy dress shoes ||| find dress shoes
i want nike shoes for my daughter ||| find nike shoes
the search for gold watches ||| find gold watches
search for galaxy note ||| find galaxy note
find the playstation four ||| find playstation four
search for a samsung galaxy note ||| find samsung galaxy note
search for sense and galaxy note ||| find sense and galaxy note
# what does an iPhone 5 cost ||| cost iPhone 5
# what does an iPhone five cost ||| cost iPhone five
# what is the status of my current bids ||| status my current bids
# what's the best price for a blue iphone cover ||| best_price blue iphone cover
# choose the blue iphone cover ||| select blue iphone cover
show me blue iphone covers ||| find blue iphone covers
# i want the blue iphone cover ||| select blue iphone cover
# select the third item ||| select third item
# show me the status of my selling items ||| status my selling items
# read my new messages to me ||| read new messages
# read my last message ||| read last message
# sell an item ||| sell item
# pick the third item ||| select third item
i want to buy an iphone 6s ||| find iphone 6s
i want to buy an iphone six s ||| find iphone six s
can you search for an iphone 6 ||| find iphone 6
can you search for an iphone six ||| find iphone six
find me an iphone ||| find iphone
search iphone 6 ||| find iphone 6
search iphone six ||| find iphone six
search for an iphone ||| find iphone
# what are today's deals
# what are today's deals in fashion
please search for an iphone 6 ||| find iphone 6
please search for an iphone six ||| find iphone six
# can you call me a taxi ||| call taxi
I am looking for an iPhone 5 ||| find iPhone 5
I am looking for an iPhone five ||| find iPhone five
# show me my last purchases
search for iPhone 5 ||| find iPhone 5
search for iPhone five ||| find iPhone five
show me products in the category women's shoes ||| find products in the category women's shoes
show me red size 8 women's shoes ||| find red size 8 women's shoes
show me red size eight women's shoes ||| find red size eight women's shoes
# what is the weather in cairo
looking for nike shoes ||| find nike shoes
i'm looking for some nike shoes ||| find nike shoes
find me some nike shoes ||| find nike shoes
can you find me an apple iphone 5s ||| find apple iphone 5s
can you find me some apple iphone phone ||| find apple iphone phone
can you find me an apple iphone 5s? ||| find apple iphone 5s
can you find me an apple iphone 5s ? ||| find apple iphone 5s
find me    good running shoes ||| find running shoes
Please    find me    running shoes ||| find running shoes
Just     find me    shoes for running ||| find shoes for running
Can you    find me    running shoes from nike ||| find running shoes from nike
find    good running shoes ||| find running shoes
Please find running shoes ||| find running shoes
Just find shoes for running ||| find shoes for running
Can you find running shoes from adidas ||| find running shoes from adidas
show me new shoes for dancing ||| find new shoes for dancing
Please show me shoes for dancing ||| find shoes for dancing
Just show me dancing shoes ||| refine dancing shoes
Can you show me Alma Danza dance shoes ||| find Alma Danza dance shoes
show new shoes for dancing ||| find new shoes for dancing
Please show shoes for dancing ||| find shoes for dancing
Just show dancing shoes ||| refine dancing shoes
Can you show Alma Danza dance shoes ||| find Alma Danza dance shoes
get me Adidas Woman running shoes ||| find Adidas Woman running shoes
Please get me shoes for gym ||| find shoes for gym
Just get me white casual shoes ||| find white casual shoes
Can you get me good hiking shoes ||| find hiking shoes
I need Adidas Woman running shoes ||| find Adidas Woman running shoes
I need shoes for gym ||| find shoes for gym
I need white casual shoes ||| find white casual shoes
I need good hiking shoes ||| find hiking shoes
I want hiking shoes under 50 dollar ||| find hiking shoes under 50 dollar
I want casual shoes ||| find casual shoes
I want athletic shoes ||| find athletic shoes
I want cross training shoes ||| find cross training shoes
I want to see woman cross trainers ||| find woman cross trainers
I want to see woman basketball shoes ||| find woman basketball shoes
I want to see men cross trainers ||| find men cross trainers
I want to see men basketball shoes ||| find men basketball shoes
I want to find shoes for walking ||| find shoes for walking
I want to find shoes for dress ||| find shoes for dress
I want to find athletic shoes ||| find athletic shoes
I want to find construction work shoes ||| find construction work shoes
I want to see woman cross trainers ||| find woman cross trainers
I want to see woman basketball shoes ||| find woman basketball shoes
I want to see men cross trainers ||| find men cross trainers
I want to see men basketball shoes ||| find men basketball shoes
I want to find shoes for walking ||| find shoes for walking
I want to find shoes for dress ||| find shoes for dress
I want to find athletic shoes ||| find athletic shoes
I want to find construction work shoes ||| find construction work shoes
Can I see cross training shoes ||| find cross training shoes
Can I see bowling shoes ||| find bowling shoes
Can I see basketball shoes ||| find basketball shoes
Can I see hiking shoes ||| find hiking shoes
Can I get casual shoes ||| find casual shoes
Can I get athletic shoes ||| find athletic shoes
Can I get construction work shoes ||| find construction work shoes
Can I get running shoes ||| find running shoes
search for prom shoes ||| find prom shoes
search for wedding shoes ||| find wedding shoes
search for beach shoes ||| find beach shoes
search for party shoes ||| find party shoes
Please search for bridal shoes ||| find bridal shoes
Please search for shoes for the club ||| find shoes for the club
Please search for shiny party shoes ||| find shiny party shoes
Please search for sandals ||| find sandals
Can you get open toe high heels ||| find open toe high heels
Can you get platform heels ||| find platform heels
Can you get plattform shoes ||| find plattform shoes
Can you get platform boots ||| find platform boots
I'd like to have Stilettos ||| find Stilettos
I'd like to have Stiletto high heels ||| find Stiletto high heels
I'd like to have shoes for walking ||| find shoes for walking
I'd like to have shoes for dress ||| find shoes for dress
I'd like to see new sneakers for the gym ||| find new sneakers for the gym
I'd like to see boots ||| find boots
I'd like to see winter boots ||| find winter boots
I'd like to see flats ||| find flats
Give me Cowboy boots ||| find Cowboy boots
Give me Leather Boots ||| find Leather Boots
Give me Zipper Heels ||| find Zipper Heels
Give me Baby shoes ||| find Baby shoes
Do you have kids shoes ||| find kids shoes
Do you have kids casual shoes ||| find kids casual shoes
Do you have kids sandals ||| find kids sandals
Do you have boy sneakers ||| find boy sneakers
Do you got girl sneakers ||| find girl sneakers
Do you got nike air for boys ||| find nike air for boys
Do you got winter boots ||| find winter boots
Do you got flip flops ||| find flip flops
