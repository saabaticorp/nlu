import ConfigParser, os
import json
import sys
import re

from corenlp import StanfordCoreNLP

#TODO load from config file
#some global params
DEBUG = 0
configFile = 'Interpreter/corenlp.proporties'
testFile   = 'Interpreter/test_sentences.v2.txt.f'
SEARCHACTION = "find"
FILTERACTION = "refine"

searchIntent = {
                'search', 
                'find', 
                'look',
                'show',
                'get',
                'need',
                'want',
                'see',
                'give',
                'have',
                'buy',
                }

filterIntent = {
                'filter',
                'refine',
                'limit',
                'show',
                'see',
                'want',
                }
#words that appear in subject
#and indicate filter intent
subjFilterIntent = {
                    'just',
                    'only',
                    'one',
                    'ones',
                    }
removeFilterObj = {
                   'ones',
                   }
#to_whom
towhomFile = "Interpreter/towhom.f2.txt"
toWhomWords = {}
with open(towhomFile) as f:
    for l in f:
        l = l.strip()
        if not re.match("^#", l):
            toWhomWords[l] = 1
verbTags = {
            "VB",   # Verb, base form
            "VBD",  # Verb, past tense
            "VBG",  # Verb, gerund or present participle
            "VBN",  # Verb, past participle
            "VBP",  # Verb, non3rd person singular present
            "VBZ",  # Verb, 3rd person singular present }
            }
adjectiveTags = {
                 "JJ",  # Adjective
                 "JJR", # Adjective, comparative
                 "JJS", # Adjective, superlative
                 }
adjToRemove = {
               'good',
               'bad',
               'nice',
               'lovely',
               'beutiful',
               'convenient',
               'romantic',
               'sexy',
               'charming',
               'pretty',
               'elegant',
               'cheap',
               'cheapest',
               }


class Parser():
    def __init__(self, configFile=configFile):
        config = ConfigParser.ConfigParser()
        config.read(configFile)

        self.props = dict( config.items('props') )
        self.nlp = StanfordCoreNLP('http://localhost:9000')

    def interpret(self, text):
        text = text.strip()
        nlpOutJson = self.nlp.annotate(text, properties=self.props)
        nlpOut = json.loads ( nlpOutJson )
        (rootIndex, dependencies) = findRootIndex( nlpOut )
        rootLemma = findLemma(nlpOut, rootIndex)
        (action, subj, obj, toWhom) = interpret(nlpOut, rootIndex, rootLemma, dependencies)
        #truecasedText = __truecase(nlpOut)
        res = {
                 'text': text,
                 #'truecasedText': truecasedText,
                 'action': action,
                 'object':obj,
                 # 'subject': subj,
               }
        if toWhom:
            res['toWhom'] = toWhom
        if DEBUG:
            print json.dumps(nlpOut, indent=4)
            printWordTags(nlpOut)
        return res
    
    def truecase(self, text):
        nlpOutJson = self.nlp.annotate(text, properties={'annotators':'truecase'})
        nlpOut = json.loads ( nlpOutJson )
        return getTruecase(nlpOut)


def getTruecase(nlpOut):
    tct = []
    for t in nlpOut['sentences'][0]['tokens']:
        if 'truecaseText' in t:
            #meh
            if t['truecaseText'] == 'IPHONE':
                tct.append('iPhone')
            else:
                tct.append(t['truecaseText'])
        else:
            tct.append(t['originalText'])
    return ' '.join(tct)

def printWordTags(nlpOut):
    for t in nlpOut['sentences'][0]['tokens']:
        print "%s/%s" % (t['word'], t['pos']), 
    print ""
    
#create triplets 
"""
Extract dependency triples of the form:
((head word, head tag), rel, (dep word, dep tag))
"""
def findRootIndex(nlpOut):
    (rootWord, rootIndex) = ("", 0)
    depTriplets = []
    dependencies = {}
    prev = None
    for i in nlpOut['sentences'][0]['collapsed-ccprocessed-dependencies']:
        lemma = nlpOut['sentences'][0]['tokens'][i['dependent']-1]['lemma']
        if i['dep'] == 'ROOT':
            rootWord  = i['dependentGloss']
            rootIndex = i['dependent']
        #please followed by search
        elif ( ( i['dependentGloss'] in searchIntent or lemma in searchIntent)
               and prev and prev['dep']=='ROOT' ):
            rootWord  = i['dependentGloss']
            rootIndex = i['dependent']
        #i want to see shoes
        elif ( (i['dependentGloss'] in searchIntent or lemma in searchIntent)
               and (i['dep']=='xcomp' or i['dep']=='ccomp' or i['dep']=='compound')
               and i['governor'] == rootIndex ):
            rootWord  = i['dependentGloss']
            rootIndex = i['dependent']
        depTriplets.append( ( i['governorGloss'], i['dep'], i['dependentGloss']))
        dependencies[i['dependentGloss']] = i['governorGloss']
        prev = i
    if DEBUG:
        print "rootIndex=%d" % rootIndex
        for d in depTriplets:
            print d
    return (rootIndex, dependencies)

#look what depends on root, split to subj, obj
#for the obj, add what depends on it
def interpret(nlpOut, rootIndex, rootLemma, dependencies):
    subj = []
    obj  = []
    root = ()
    ind = 1
    includesSubjFilterIntent = False
    toWhom = ""
    for i in nlpOut['sentences'][0]['tokens']:
        if i['index'] < rootIndex:
            if i['lemma'] in subjFilterIntent:
                includesSubjFilterIntent = True
            subj.append( (i['word'], i['pos']) )
        elif i['index'] == rootIndex:
            root = ( (i['word'], i['pos']) )
        else: # i['index'] > rootIndex:
            #discard towhom
            if i['lemma'] in toWhomWords and toWhom:
                toWhom = i['word']
            elif i['word'] in dependencies and dependencies[i['word']] in toWhomWords:
                if i['word'] == "for" or i['word'] == "to":
                    toWhom = i['word']
            #discard IN/DT at beginning of obj span
            elif obj or (not obj and i['pos']!='IN' and i['pos']!='DT' and i['pos']!='PRP' and i['lemma'] not in subjFilterIntent):
                obj.append( (i['word'], i['pos']) )
            if i['lemma'] in subjFilterIntent:
                includesSubjFilterIntent = True
        ind += 1
    #remove trailing punc in obj
    i=len(obj)-1
    while i>0:
        if obj[i][1] == '.':
            obj.pop()
        else:
            break
        i-=1
    #remove trailing words like "ones" when intent is refine
    i=len(obj)-1
    while i>0:
        if obj[i][0] in removeFilterObj:
            obj.pop()
        else:
            break
        i-=1
    #action
    if rootLemma in searchIntent and rootLemma in filterIntent and includesSubjFilterIntent:
        action=FILTERACTION
    elif rootLemma in searchIntent:
        action=SEARCHACTION
    elif rootLemma in filterIntent:
        action=FILTERACTION
    elif includesSubjFilterIntent:
        action=FILTERACTION
        obj.insert(0, root)
        #TODO add all connected to root and remove func words
    else:
        print "WARNING: unknown intent"
        action=SEARCHACTION        
        #some logic if root is not a verb
        if root[1] not in verbTags:
            subj=[]
            obj=[]
            for t in nlpOut['sentences'][0]['tokens']:
                obj.append((t['word'], t['pos']))
        #action="unknown"
    
    #remove adjectives from beinning of object
    obj = removeAdjFromStart(obj)

    subj = ' '.join([w for w,t in subj])
    objs = ""
    for w,t in obj:
        if t == 'POS':
            objs += w
        else:
            objs += ' '+w
    objs = objs.strip()

    return (action, subj, objs, toWhom)

def removeAdjFromStart(obj):
    while obj:
        if obj[0][1] in adjectiveTags and obj[0][0] in adjToRemove:
            obj.pop(0)
        else:
            break
    return obj

def findLemma(nlpOut, index):
    return nlpOut['sentences'][0]['tokens'][index-1]['lemma']


testIntepreter = []
if DEBUG:
    with open(testFile) as f:
        for l in f:
            l = l.strip()
            if not re.match("^#", l):
                query_result = l.split(" ||| ")
                testIntepreter.append(query_result)


def main():
    myI = Parser()
    global DEBUG
    if DEBUG == 1:
        DEBUG = 0
        for t in testIntepreter:
            res = myI.interpret(t[0])
            comp = res['action']+" "+res['object']
            if comp != t[1]:
                print "ERROR: assetion failed for sentence:[%s], parse:[%s], ref:[%s]" %(t[0], comp, t[1])
        DEBUG = 1
    print "Type sentence, press enter, then ctrl+d..."        
    for text in sys.stdin:
        #print json.dumps(myI.truecase(text), indent=4)
        print json.dumps(myI.interpret(text), indent=4)




if __name__ == "__main__":
    main()
    sys.exit(0)