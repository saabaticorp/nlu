import ConfigParser, os
import json
import sys
import re

from corenlp import StanfordCoreNLP
from parser import Parser, SEARCHACTION
from text2num import text2num as getText2num

sys.path.append(sys.path[0] + '/../SpecTagger')
from lr_tagger import Tagger 

#some global params
DEBUG = 0
CORENLPCONFIG = 'Models/corenlp.proporties'
TAGGERMODEL   = "Models/tagger.lr.20022016.model"

class NLU():
    def __init__(self):
        self.parser = Parser(configFile=CORENLPCONFIG)
        self.tagger = Tagger(TAGGERMODEL)
        #warm up the parser
        #parser has lazy model loading, so
        #a call has to be done at the beginning
        self.parse('wake up dude, lets do it!')
    
    def tag(self, text):
        return self.tagger.tag(text)
    
    def parse(self, text):
        return self.parser.interpret(text)
    
    def interpret(self, text, history=None):
        text = text.strip()
        textn = self.text2num(text)
        parse_json = self.parse(textn)
        object = parse_json['object']
        #fall back to text if parse failed
        if object == "":
            object = text
        tag_json = self.tag(object)
        #truecase
        #stanford not working well...
        #parse_json['postprocessed-text'] = self.uppercaseFirst(textn)
        #if no history set intent to find
        if not history:
            parse_json['action'] = SEARCHACTION
        """
        if history:
            #TODO check if already in there
            tags = []
            for t in history['tagger']['tags']:
                tags.append(t)
            for t in tag_json['tags']:
                tags.append(t)
            tag_json['tags'] = tags
            parse_json['object'] = history['interpreter']['object'] + " " + parse_json['object']
        """
        i_json = {'text': text,
                  'postprocessed-text': self.uppercaseFirst(textn),
                  'interpreter':parse_json,
                  'tagger':tag_json
                  }
        if history:
            i_json['history'] = history
        return i_json
    
    def text2num(self, text):
        return getText2num(text)
    
    def uppercaseFirst(self, text):
        return text[0].upper() + text[1:]
    
    def truecaseUsingTagger(self, text, tag_json):
        text = self.stanfordTruecase(text)
        tokens = text.split()
        tokenso = []
        ttd = {}
        for tt in tag_json['tags']:
            ttd[tt['token']] = tt['tag']
        for t in tokens:
            tl = t.lower()
            #lowercase color
            if tl in ttd and ttd[tl] == 'color':
                tokenso.append(tl)
            else:
                tokenso.append(t)
        return ' '.join(tokenso)
        
    def stanfordTruecase(self, text):
        return self.parser.truecase(text)

def main():
    nlu = NLU()
    text = "i want nike shoes"
    turn1 = nlu.interpret(text)
    print json.dumps(turn1, indent=4)
    #text = "just show red ones"
    text = "show only baby"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    text = "show red ones"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    text = "show me red ones"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    text = "only show red shoes"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    text = "show only red shoes"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    
    print "###########################"
    print "###########################"
    
    text = "i want apple iphone five s"
    turn1 = nlu.interpret(text)
    print json.dumps(turn1, indent=4)
    #text = "just show red ones"
    text = "show only 64GB"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    
    print "###########################"
    print "###########################"
    
    text = "i want a forty three inch samsung tv"
    turn1 = nlu.interpret(text)
    print json.dumps(turn1, indent=4)
    #text = "just show red ones"
    text = "only want 1080p resolution"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    
    print "###########################"
    print "###########################"
    
    text = "I want to buy running shoes."
    turn1 = nlu.interpret(text)
    print json.dumps(turn1, indent=4)
    #text = "just show red ones"
    text = "Only show blue shoes"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    text = "Only show for baby"
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
        
    print "###########################"
    print "###########################"
    text = "My daughter wants to buy high heels."
    turn1 = nlu.interpret(text)
    print json.dumps(turn1, indent=4)
    #text = "just show red ones"
    text = "Show me only those with open toes."
    turn2 = nlu.interpret(text, turn1)
    print json.dumps(turn2, indent=4)
    print "###########################"
    text = "show only those with ankle strap"
    turn3 = nlu.interpret(text, turn2)
    print json.dumps(turn3, indent=4)
    print "###########################"
    
    print "Ready for input..."
    for text in sys.stdin:
        print json.dumps(nlu.interpret(text), indent=4)




if __name__ == "__main__":
    main()
    sys.exit(0)