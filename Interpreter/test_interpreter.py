import pytest

import interpreter

interp = interpreter.Interpreter()

@pytest.mark.parametrize("test_input,expected", [
    ('looking for nike shoes', "{'action': 'search', 'obj': u'for nike shoes ', 'subj': ''}"),
])

def test_interpret(test_input, expected):
    assert interp.interpret(test_input) == expected