"""
Logistic regression classifier

The classifier uses standard features, including a combination
of words, tags, prefixes and suffixes

Word embeddings implementation ready
"""
from collections import defaultdict
import pickle
import random
import logging
import re

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction import DictVectorizer
from scipy import sparse

import numpy as np


class LRTagger():
    def __init__(self, donormalize=True, word_model=None, num_threads=4):
        self.model = LogisticRegression()
        self.classes = []
        self.tagdict = {}
        self.donormalize = donormalize
        self.word_model = word_model
        self.num_jobs = num_threads
        self.vectorSize = 0
        self.dictVec = {}
        if word_model:
            self.vectorSize = int(re.search('size=(\d+)', word_model.__str__()).group(1))  #word_model.vector_size
        self.zeroV = np.zeros(self.vectorSize)
        self.numWV = 3  #number of word vectors, w-1, w, w+1
        self.normalizer = None  #wordvec l2 normalizer
            
    def train(self, trainFile, num_iters=5):
        sentences = read_data(trainFile)
        
        training_features = []
        wordvecFeatures = []
        training_labels = []
        
        self._make_tagdict(sentences)
        print self.classes
        print "Number of possible tags: ", len(self.classes)
        
        numSentence = 0
        for words, tags in sentences:
            avgVector = self.zeroV
            numAdded = 0
            #tag_{i-2}, tag{i-1}
            prevTags = [None, None]            
            for i in range(len(words)):
                feats = self._get_features(i, words[i], words, prevTags)
                training_features.append(feats)
                #add word2vec features
                if self.word_model:
                    wv = None
                    if not words[i] in self.word_model: 
                        wv=np.zeros(self.vectorSize*self.numWV)
                    else:
                        wv = self._get_wordvec_features(words,i)
                        avgVector += self.word_model[words[i]]
                        numAdded += 1
                    wordvecFeatures.append(wv)
                tagI = self.classes.index(tags[i])
                training_labels.append(tagI)
                prevTags[0] = prevTags[1]                
                prevTags[1] = tags[i]
            numSentence += 1

        self.dictVec = DictVectorizer(sparse=True)
        feature_matrix_lexical = self.dictVec.fit_transform(training_features)
        label_vector = np.asarray(training_labels)
        
        if self.word_model:
            self.normalizer = preprocessing.Normalizer().fit(wordvecFeatures)  # fit does nothing
            feature_matrix = sparse.hstack([feature_matrix_lexical, self.normalizer.transform(wordvecFeatures)])
        else:
            feature_matrix = feature_matrix_lexical
        print "features shape=", feature_matrix.shape
        print "labels shape=", label_vector.shape

        #try def LR
        print "Training logistic regression classifier."
        self.model.fit(feature_matrix, label_vector)

    def tag(self, words):
        predictedTags = []
        training_features = []
        numAdded = 0
        avgVector = self.zeroV

        prevTags = [None, None]            
        for i, word in enumerate(words):
            tag = self.tagdict.get(word)
            if not tag:
                feats = self._get_features(i, word, words, prevTags)
                feature_matrix_lexical = self.dictVec.transform(feats)
                #add word2vec features
                if self.word_model:
                    wv = None
                    if not words[i] in self.word_model: 
                        wv=np.zeros(self.vectorSize*self.numWV)
                    else:
                        wv = self.normalizer.transform( np.array(self._get_wordvec_features(words,i)).reshape(1, -1) )
                    feature_matrix = sparse.hstack([feature_matrix_lexical, wv])
                else:
                    feature_matrix = feature_matrix_lexical
                tagI = self.model.predict(feature_matrix)
                prevTags[0] = prevTags[1]
                prevTags[1] = self.classes[tagI]
                tag = self.classes[tagI]
            predictedTags.append(tag)
        return predictedTags
   
    def _get_wordvec_features(self, words, i):
        currFeatures = []
        if i==0:
            if '<s>' in self.word_model:
                currFeatures.extend(self.word_model['<s>'])
            else:
                currFeatures.extend(self.zeroV)
        else:
            if words[i-1] in self.word_model:
                currFeatures.extend(self.word_model[words[i-1]])
            else:
                currFeatures.extend(self.zeroV)
        currFeatures.extend(self.word_model[words[i]])
        if i==len(words)-1:
            currFeatures.extend(self.word_model['</s>'])
        else:
            if words[i+1] in self.word_model:
                currFeatures.extend(self.word_model[words[i+1]])
            else:
                 currFeatures.extend(self.zeroV)
        return currFeatures    


    """
    get lexical features
    features include tag{i-1}, tag{i-2}
    some feature engineering
    """
    def _get_features(self, i, word, context, prevTags):
        def add(name, *args):
            features[' '.join((name,) + tuple(args))] += 1
        
        prev2   = prevTags[0]
        prev    = prevTags[1]
        
        features = defaultdict(int)
        add('bias')
        add('i suffix3', word[-3:])
        #add('i suffix2', word[-2:])
        #add('i suffix1', word[-1:])
        add('i prefix1', word[:1])
        add('i prefix2', word[:2])
        add('i prefix3', word[:3])
        add('i word', context[i])
        if i>0:
            add('i-1 word', context[i-1])
            add('i-1 suffix3', context[i-1][-3:])
            add('i-1 prefix1', context[i-1][:1])
            if i>1:
                add('i-2 word', context[i-2])
        if i<len(context)-1:
            add('i+1 word', context[i+1])
            add('i+1 suffix3', context[i+1][-3:])
            add('i+1 prefix1', context[i+1][:1])
            if i<len(context)-2:
                add('i+2 word', context[i+2])
        addtagfeatures = 1
        if addtagfeatures:
            if prev:
                #add('i-1 tag', prev)
                add('i-1 tag+i word', prev, context[i])                
                #if i>0:
                #    add('i-1 tag+i-1 word', prev, context[i-1])
                #if i<len(context)-1:
                #    add('i-1 tag+i+1 word', prev, context[i+1])
                if prev2:
                    add('i-2 tag+i word', prev2, context[i])
                    if i>0:
                        add('i-2 tag+i-1 word', prev2, context[i-1])
                        #if i>1:
                        #    add('i-2 tag+i-2 word', prev2, context[i-2])
                    #add('i-1 tag+i-2 tag', prev, prev2)
                    #add('i-1 tag+i-2 tag+ i word', prev, prev2, context[i])
            #if prev2:
            #    add('i-2 tag', prev2)
        shapefeatures = 1
        if shapefeatures:
            #no dash
            #if i<len(context)-1 and not re.search('-', context[i+1]):
            #    add('i+1 NoDash')
            if not re.search('-', context[i  ]):
                add('i NoDash')
            #if i>0 and not re.search('-', context[i-1]):
            #    add('i-1 NoDash')
            #no number
            #if not re.search('[0-9]', context[i+1]):
            #    add('i+1 NoNum')
            #if not re.search('[0-9]', context[i ]):
            #    add('i NoNum')
            #if not re.search('[0-9]', context[i-1]):
            #    add('i-1 NoNum')
            #no punc
            #if i<len(context)-1 and not re.search('[%\-\#_\)?"&.+*\]\(\'|!\/:\[$=~,]', context[i+1]):
            #    add('i+1 NoPunc')
            if not re.search('[%\-\#_\)?"&.+*\]\(\'|!\/:\[$=~,]', context[i  ]):
                add('i NoPunc')
            #if i>0 and not re.search('[%\-\#_\)?"&.+*\]\(\'|!\/:\[$=~,]', context[i-1]):
            #    add('i-1 NoPunc')
        
        return features
    
    """
    tag dictionary
    this is created for unambigious words in training data
    """
    def _make_tagdict(self, sentences):
        counts = defaultdict(lambda: defaultdict(int))
        classesSet = set()
        for words, tags in sentences:
            for word, tag in zip(words, tags):
                counts[word][tag] += 1
                classesSet.add(tag)
        self.classes = list(classesSet)
        freq_thresh = 20
        ambiguity_thresh = 0.97
        for word, tag_freqs in counts.items():
            tag, mode = max(tag_freqs.items(), key=lambda item: item[1])
            n = sum(tag_freqs.values())
            if n >= freq_thresh and (float(mode) / n) >= ambiguity_thresh:
                self.tagdict[word] = tag

    def save(self, modelDump):
        pickle.dump((self.model, self.dictVec, self.tagdict, self.classes),
                     open(modelDump, 'wb'), -1)
    
    #Load a pickled model
    def load(self, filename):
        try:
            pload = pickle.load(open(filename, 'rb'))
        except IOError:
            msg = ("Missing pickle file.")
            print msg
            raise
        self.model, self.dictVec, self.tagdict, self.classes = pload
        return None



def normalize(word, donormalize=True):
    if donormalize:
        return word.lower()
    return word

def read_data(filename, donormalize=True):
    sentences = []
    with open(filename) as f:
        wordsTags = ([],[])
        for line in f:
            tokens = line.split()
            if tokens:
                if donormalize:
                    wordsTags[0].append(normalize(tokens[0]))
                else:
                    wordsTags[0].append(tokens[0])
                m=re.match('^(\w+),',tokens[1])
                if m:
                    wordsTags[1].append( m.group(1) )
                else:
                    wordsTags[1].append(tokens[1])
            else:
                sentences.append(wordsTags)
                wordsTags = ([],[])
        if wordsTags:
            sentences.append(wordsTags)
        return sentences
