# README #

OBSOLETE, now under  
https://bitbucket.org/omnifluent/nlu/  
AppTek/Shopping/NLU  

NLU

### What is this repository for? ###

* NLU includes compenents to perform language understanding
 * Preprocessor
 * SpecTagger
 * Interpreter
 * Models - where the models/config lives. Download model from [here](https://drive.google.com/file/d/0B61yGZ67Jb0NSmFqVWtzOFlITUE/view?usp=sharing), rename to tagger.lr.20022016.model
* Version
* [API with Gateway](https://omnifluent.atlassian.net/browse/SA-40)
* [DIVA architecture][https://omnifluent.atlassian.net/wiki/pages/viewpage.action?pageId=1114159]

### How do I get set up? ###

* check out README in SpecTagger and Interpreter
* Configuration
* Dependencies
* How to run tests

start a corenlp server
```
java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer
```
run NLU
```
python Interpreter/interpreter.py
looking for apple iphone 5s
{
    "interpreter": {
        "action": "find", 
        "text": "looking for apple iphone 5s", 
        "object": "apple iphone 5s"
    }, 
    "tagger": {
        "text": "apple iphone 5s", 
        "tags": [
            {
                "token": "apple", 
                "tag": "brand"
            }, 
            {
                "token": "iphone", 
                "tag": "model"
            }, 
            {
                "token": "5s", 
                "tag": "model"
            }
        ]
    }
}
```

### Who do I talk to? ###

* Saib